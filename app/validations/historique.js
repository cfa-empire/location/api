const Joi = require('joi');

module.exports = {
  create: {
    position: Joi.any().optional(),
    message: Joi.string().optional(),
    date: Joi.date().optional(),

    location: Joi.string().optional(),
    admin: Joi.string().optional(),
  },
  update: {
    position: Joi.any().optional(),
    message: Joi.string().optional(),
    date: Joi.date().optional(),

    location: Joi.string().optional(),
    admin: Joi.string().optional(),
  },
  remove: {
    id: Joi.string().required()
  },
}