const Joi = require('joi');

module.exports = {
    register: {
        password: Joi.string().required(),
        phone: Joi.string().required(),
        role: Joi.string().required(),
        
        address: Joi.any().optional(),
        addressAgence: Joi.any().optional(),

        fullname: Joi.string().optional(),
        name: Joi.string().optional(),
        ifu: Joi.string().optional(),
        rccm: Joi.string().optional(),
        adress: Joi.string().optional(),
        active: Joi.boolean().optional(),


        gerantFullname: Joi.string().optional(),
        gerantPhone: Joi.string().optional(),
        gerantEmail: Joi.string().optional(),
        gerantPieceType:Joi.string().optional(),
        gerantPieceNumber: Joi.string().optional(),
        
    },
    login: {
        phone: Joi.string().required(),
        password: Joi.string().required(),
    },
    update: {
        password: Joi.string().optional(),
        fullname: Joi.string().optional(),
        phone: Joi.string().optional(),
        role: Joi.string().optional(),
        statut: Joi.boolean().optional(),
        
        address: Joi.any().optional(),
        addressAgence: Joi.string().optional(),

        name: Joi.string().optional(),
        ifu: Joi.string().optional(),
        rccm: Joi.string().optional(),
        adress: Joi.string().optional(),
        phone: Joi.string().optional(),
        active: Joi.boolean().optional(),

        gerantFullname: Joi.string().optional(),
        gerantPhone: Joi.string().optional(),
        gerantEmail: Joi.string().optional(),
        gerantPieceType:Joi.string().optional(),
        gerantPieceNumber: Joi.string().optional(),
    },
    remove: {
        id: Joi.string().required()
    }

    /*
    login_confirm: {
        email: Joi.string().required(),
        password: Joi.string().required(),
        codeparrainage: Joi.string().required()
    },*/
}