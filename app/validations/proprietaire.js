const Joi = require('joi');

module.exports = {
  create: {
    fullname: Joi.string().required(),
    email: Joi.string().required(),
    phone: Joi.string().required(),
    agence: Joi.string().required(),
  },
  update: {
    fullname: Joi.string().optional(),
    email: Joi.string().optional(),
    phone: Joi.string().optional(),
    agence: Joi.string().optional(),
  },
  remove: {
    id: Joi.string().required()
  },
}