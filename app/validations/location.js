const Joi = require('joi');

module.exports = {
  create: {
    address: Joi.any().required(),
    price: Joi.number().required(),
    zone: Joi.string().required(),
    message: Joi.string().required(),
    description: Joi.object().required(),

    proprietaire: Joi.string().required(),
    agence: Joi.string().required(),
  },
  createWithFile: {
    fileprop: Joi.any().optional(),  
    filelinks: Joi.any().optional(),  
    filenumber: Joi.number().optional(),  
    filedata0: Joi.any().optional(), 
    filedata1: Joi.any().optional(), 
    filedata2: Joi.any().optional(), 
    filedata3: Joi.any().optional(), 
    objectdata: Joi.any().optional(),
  },
  update: {
    address: Joi.any().optional(),
    price: Joi.number().optional(),
    zone: Joi.string().optional(),
    message: Joi.string().optional(),
    description: Joi.object().optional(),

    proprietaire: Joi.string().optional(),
    agence: Joi.string().optional(),
  },
  remove: {
    id: Joi.string().required()
  },
}