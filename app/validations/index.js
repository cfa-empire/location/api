const admin = require('./admin');

const proprietaire = require('./proprietaire');
const location = require('./location');
const historique = require('./historique');

module.exports = {
  admin,

  proprietaire,
  location,
  historique,
};