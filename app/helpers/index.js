const UserHelper = require('./user');
const ResourceHelper = require('./resource');

module.exports = {
  admin: new UserHelper('Admin', 'phone', 'agence'),

  proprietaire: new ResourceHelper('Proprietaire', 'agence ', '-__v'),
  location: new ResourceHelper('Location', 'agence', '-__v'),
  historique: new ResourceHelper('Historique', '', '-__v'),
};