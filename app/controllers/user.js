'use strict';
var JWT = require('jsonwebtoken');
const Config = require('../../config/config');
const MainHelper = require('../helpers');

module.exports = class UserController {
    constructor(type, plural, validate) {
        this.type = type;
        this.plural = plural;
        this.validate = validate;
    }

    getDetails() {
        return {
            description: 'Returns the ' + this.type + ' info',
            auth: 'jwt',
            handler: async (request, h) => {
                try {
                    return h.response({
                        [this.type]: await MainHelper[this.type].findUserDetails(request.headers.userId)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    get() {
        return {
            description: 'Returns the ' + this.type + ' info',
            auth: 'jwt',
            handler: async (request, h) => {
                try {
                    return h.response({
                        [this.type]: await MainHelper[this.type].get(request.params.user)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    count() {
        return {
            description: 'Returns the count of ' + this.plural,
            auth: 'jwt',
            handler: async (request, h) => {
                try {
                    if (request.query.createdAt) {
                        request.query.createdAt = JSON.parse(request.query.createdAt);
                    }

                    return h.response({
                        [this.plural]: await MainHelper[this.type].count(request.query)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    listAuth() {
        return {
            description: 'Returns the list of ' + this.total,
            auth: 'jwt',
            handler: async (request, h) => {
                try {

                    let query = request.query.options ? JSON.parse(request.query.options) : {};
                    let filter = {};
                    let sort = {};

                    if (request.query.filter) {
                        const filterData = JSON.parse(request.query.filter);
                        if (filterData.text) {
                            filter = { $or: [] };

                            filterData.type.forEach(type => {
                                filter['$or'].push({
                                    [type]: new RegExp(filterData.text, 'gi')
                                });
                            });
                        }
                    }

                    if (request.query.sort) {
                        let sortData = JSON.parse(request.query.sort);
                        for (var p in sortData) {
                            if (sortData[p] !== 0) {
                                sort[p] = sortData[p];
                            }
                        }
                    }

                    const params = {
                        ...query,
                        ...filter
                    };

                    const page = request.query.page ? parseInt(request.query.page) : 0;
                    const perPage = request.query.perPage ? parseInt(request.query.perPage) : 30;

                    let users = await MainHelper[this.type].list(params, page, perPage, sort);
                    let userCount = await MainHelper[this.type].count(params);

                    return h.response({
                        [this.plural]: users,
                        total: userCount
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    list() {
        return {
            description: 'Returns the list of ' + this.total,
            auth: false,
            handler: async (request, h) => {
                try {

                    let query = request.query.options ? JSON.parse(request.query.options) : {};
                    let filter = {};
                    let sort = {};

                    if (request.query.filter) {
                        const filterData = JSON.parse(request.query.filter);
                        if (filterData.text) {
                            filter = { $or: [] };

                            filterData.type.forEach(type => {
                                filter['$or'].push({
                                    [type]: new RegExp(filterData.text, 'gi')
                                });
                            });
                        }
                    }

                    if (request.query.sort) {
                        let sortData = JSON.parse(request.query.sort);
                        for (var p in sortData) {
                            if (sortData[p] !== 0) {
                                sort[p] = sortData[p];
                            }
                        }
                    }

                    const params = {
                        ...query,
                        ...filter
                    };

                    const page = request.query.page ? parseInt(request.query.page) : 0;
                    const perPage = request.query.perPage ? parseInt(request.query.perPage) : 30;

                    let users = await MainHelper[this.type].list(params, page, perPage, sort);
                    let userCount = await MainHelper[this.type].count(params);

                    return h.response({
                        [this.plural]: users,
                        total: userCount
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    registerWithFile() {
        return {
            description: 'Create a new ' + this.type,
            auth: false,
            payload: {
                output: 'stream',
                allow: 'multipart/form-data', // important,
                maxBytes: 30 * 1024 * 1024
            },
            validate: {
                payload: this.validate.registerWithFile,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async (request, h) => {
                console.log('in register');
                try {
                    if (request.payload.parrain) {
                        let dataParrain = await MainHelper['admin'].list({ codeparrainage: request.payload.parrain });
                        if (dataParrain) {
                            request.payload.parrain = dataParrain[0]._id
                        } else {
                            return h.response({ message: "Code de parrainage invalide." }).code(400);
                        }
                    }

                    let data = await MainHelper[this.type].register({ ...request.payload });
                    return h.response({ message: data.message, user: data.user }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    register() {
        return {
            description: 'Create a new ' + this.type,
            auth: false,
            validate: {
                payload: this.validate.register,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async (request, h) => {
                console.log('in register');
                try {
                    let data = await MainHelper[this.type].register({ ...request.payload });
                    return h.response({ message: data.message, user: data.user }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    login() {
        return {
            description: 'Login to your account',
            auth: false,
            validate: {
                payload: this.validate.login,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async (request, h) => {
                try {
                    let data = await MainHelper[this.type].findByCredentials(request.payload.phone, request.payload.password);

                    if (data.statusCode === 200) {
                        let secret = Config.get('/jwtAuthOptions/key');

                        let obj = {
                            userId: data.user.id
                        };

                        let jwtToken = JWT.sign(obj, secret, { expiresIn: '7 days' });

                        data.user.password = undefined;
                        data.user.salt = undefined;

                        var response = h.response({ message: 'Successfully login', [this.type]: data.user, token: jwtToken });

                        response.header('Authorization', jwtToken);
                        response.code(200);

                        return response;
                    } else {
                        return h.response({ message: data.message }).code(data.statusCode);
                    }
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    loginconfirm() {
        return {
            description: 'Login to confirm your account',
            auth: false,
            validate: {
                payload: this.validate.login_confirm,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async (request, h) => {
                try {
                    console.log(request.payload.email, request.payload.password, request.payload.codeparrainage)
                    let data = await MainHelper[this.type].findByCredentials(request.payload.email, request.payload.password);

                    if (data.statusCode === 200) {
                        if (request.payload.codeparrainage == data.user.codeparrainage) {
                            let secret = Config.get('/jwtAuthOptions/key');

                            let obj = {
                                userId: data.user.id
                            };

                            let jwtToken = JWT.sign(obj, secret, { expiresIn: '7 days' });

                            data.user.password = undefined;
                            data.user.salt = undefined;


                            let response = h.response({ message: 'Successfully login', [this.type]: data.user, token: jwtToken });

                            response.header('Authorization', jwtToken);
                            response.code(200);
                            return response;
                        } else {
                            let response = h.response({ message: 'Failed Confirm Login', [this.type]: data.user, token: jwtToken });
                            response.code(400);
                            return response;
                        }

                    } else {
                        return h.response({ message: data.message }).code(data.statusCode);
                    }
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    update() {
        return {
            description: 'Update the ' + this.type,
            auth: 'jwt',
            validate: {
                payload: this.validate.update,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async (request, h) => {
                try {
                    let userId = request.params.user;
                    let userData = request.payload;

                    //console.log("datas", userId,userData);
                    let data = await MainHelper[this.type].update(userId, userData);

                    return h.response({ message: 'Done', [this.type]: data.user }).code(200);
                } catch (error) {
                    console.log("error :", error);
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    remove() {
        return {
            description: 'Remove the ' + this.type,
            auth: 'jwt',
            validate: {
                payload: this.validate.remove,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async (request) => {
                try {
                    return await MainHelper[this.type].remove(request.payload.id);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }
};