const UserController = require('./user');
const ResourceController = require('./resource');
const validation = require('../validations');
const Mail = require('./mail');

module.exports = {
  admin: new UserController('admin', 'admins', validation.admin),

  agence: new ResourceController('agence', 'agences', validation.agence),
  proprietaire: new ResourceController('proprietaire', 'proprietaires', validation.proprietaire),
  location: new ResourceController('location', 'locations', validation.location),
  historique: new ResourceController('historique', 'historiques', validation.historique),

  mail: new Mail('mail', 'mails')
};