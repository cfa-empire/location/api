const nodemailer = require('nodemailer');

module.exports = class Pay {

  constructor (type, plural) {
    this.type = type;
    this.plural = plural;
  }

  sendMail(){
    return {
      auth: false,
      handler: async (request, h) => {
        try {
          // console.log('Notre Request Query :', request.payload);
          let resp = {};

          let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
              user: 'internet.moitie.prix@gmail.com',
              pass: 'imp00000'
            },
            tls: {rejectUnauthorized: false}
          });

          let mailOptions = {
            to: request.payload.email,
            from: 'internet.moitie.prix@gmail.com',
            subject: request.payload.name +' vous contact',
            text: 'Je vous contacte...',
            html: '<h2> <b>' + request.payload.name + '</b></h2> <br><br>'+ request.payload.message +' <br>',
          };

          transporter.sendMail(mailOptions, (err, info) => {
            if(err){return console.log('Error transporter sendmail :', err);}

            console.log('Message sent infos :', info);
            resp = info.messageId;
            // console.log('Preview URL : ', nodemailer.getTestMessageUrl(info));

          });
          
          await new Promise(resolve => setTimeout(resolve, 2000)); // 3 sec

          return h.response({
            [this.plural]: resp,
          }).code(200);
        } catch (error) {
          return error;
        }
      },
      tags: ['api']
    };
  }

  welcomeMail(){
    return {
      auth: false,
      handler: async (request, h) => {
        try {
          console.log('Notre Request Query :', request.payload);
          let resp = {};

          let transporter = nodemailer.createTransport({
            host: 'mail.privateemail.com',
            // service: 'gmail',
            port: 465,
            secure: true,
            auth: {
              user: 'support@worldbusiness.store',
              pass: 'Support4*'
            },
            tls: {rejectUnauthorized: false}
          });

          let mailOptions = {
            from: 'support@worldbusiness.store',
            // to: 'oneany600@gmail.com', 
            to: request.payload.email,
            subject: 'Bienvenue dans World Business !',
            // text: 'Hello Somebody',
            html: `
                      <li>Titulaire: <b>`+ request.payload.banque + `</b> </li>
                   `
          };

          transporter.sendMail(mailOptions, (err, info) => {
            if(err){return console.log('Error transporter sendmail :', err);}

            console.log('Message sent infos :', info);
            resp = info.messageId;
            // console.log('Preview URL : ', nodemailer.getTestMessageUrl(info));
          });
          
          await new Promise(resolve => setTimeout(resolve, 4000)); // 3 sec

          return h.response({
            [this.plural]: resp,
          }).code(200);
        } catch (error) {
          return error;
        }
      },
      tags: ['api']
    };
  }

  verificationMail(){
    return {
      auth: false,
      handler: async (request, h) => {
        try {
          console.log('Notre Request Query :', request.payload);
          let resp = {};

          let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
              user: 'internet.moitie.prix@gmail.com',
              pass: 'imp00000'
            },
            tls: {rejectUnauthorized: false}
          });

          let mailOptions = {
            from: 'internet.moitie.prix@gmail.com', 
            to: request.payload.email,
            subject: 'Activez votre nouveau compte !',
            // text: 'Hello Somebody',
            html: `<!DOCTYPE html>
            <html>
              <style type="text/css">
                .card {
                  padding: 0 30%;
                } 

                @media screen and (min-width:180px) and (max-width:750px) and (max-width:1120px){
                  .card {
                    padding: 4%;
                  }
                }
              </style>
            <body style="background-color: white; font-size: 10px;" class="card">
              <div style="color: rgb(16, 48, 107); padding: 20px; background-color: #ecebeb">
                
                <h2>Vous y êtes presque ! Il suffit de confirmer votre adresse e-mail</h2> <br>
                Bonjour et bienvenue dans le réseau Esso, `+request.payload.name+`. <br><br>
            
                Félicitation, votre compte Esso est désormais créé. Pour l'activer,
                cliquez ci-dessous pour vérifier votre adresse de courier. <br><br>
            
                <span style="text-align: center; padding: 5px; background-color: #3bab38;
                  color: white; cursor: pointer;">
                  <a href="https://esso.com/verification/code"></a>
                  Activez votre compte
                </span>
                
                <br><br>
            
                Pour tout renseignement, veuillez nous contacter aux : <br>
                
                (BJ) +229 622 395 96 <br>
                <a href="mailto:internet.moitie.prix@gmail.com">internet.moitie.prix@gmail.com</a>. <br>
            </body>
            <hr style="margin-top: 10px; margin-bottom: 10px;">
            <div style="text-align: center;">
              Cet e-mail est destiné à `+request.payload.name+`, puisque vous vous êtes inscrit à Esso | 
              Les liens figurant dans cet e-amil redirigent toujours vers
              <a href="https://esso.com">https://esso.com</a> <br>
              <span class="col-sm-4">
                &copy; 2021 <span class="yspc">esso.com</span> Tous droits réservés
              </span> 
            </div>
            </html>
            `
          };

          transporter.sendMail(mailOptions, (err, info) => {
            if(err){return console.log('Error transporter sendmail :', err);}

            console.log('Message sent infos :', info);
            resp = info.messageId;
            // console.log('Preview URL : ', nodemailer.getTestMessageUrl(info));
          });
          
          await new Promise(resolve => setTimeout(resolve, 4000)); // 3 sec

          return h.response({
            [this.plural]: resp,
          }).code(200);
        } catch (error) {
          return error;
        }
      },
      tags: ['api']
    };
  }


}