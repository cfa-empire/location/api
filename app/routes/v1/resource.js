'use strict';
const MainController = require('../../controllers');

exports.plugin = {
  register: (plugin, options) => {
    const Modules = [
      {
        path: 'proprietaires/',
        ctrl: MainController.proprietaire,
      },     
      {
        path: 'locations/',
        ctrl: MainController.location,
      },
      {
        path: 'historiques/',
        ctrl: MainController.historique,
      },
    ];

    const basePath = '/api/v1/';

    for (let i = 0; i < Modules.length; i++) {
      const mdl = Modules[i];
      const modulePath = basePath + mdl.path;

      plugin.route([
        {
          method: 'GET',
          path: modulePath + "{resource}",
          config: mdl.ctrl.get()
        },
        {
          method: 'GET',
          path: modulePath + 'list',
          config: mdl.ctrl.list()
        },
        {
          method: 'GET',
          path: modulePath + 'getById',
          config: mdl.ctrl.getById()
        },
        {
          method: 'GET',
          path: modulePath + 'listAuth',
          config: mdl.ctrl.listAuth()
        },
        {
          method: 'GET',
          path: modulePath + 'count',
          config: mdl.ctrl.count()
        },
        {
          method: 'POST',
          path: modulePath + 'create',
          config: mdl.ctrl.create()
        },
        {
          method: 'POST',
          path: modulePath + 'createWithoutAuth',
          config: mdl.ctrl.createWithoutAuth()
        },
        {
          method: 'POST',
          path: modulePath + 'create-file',
          config: mdl.ctrl.createWithFile()
        },
        {
          method: 'PUT',
          path: modulePath + "{resource}",
          config: mdl.ctrl.update()
        },
        {
          method: 'PUT',
          path: modulePath + "{resource}/file",
          config: mdl.ctrl.updateWithFile()
        },
        {
          method: 'DELETE',
          path: modulePath + 'remove',
          config: mdl.ctrl.remove()
        }
      ]);
    }

    plugin.route([
      {
        method: 'POST',
        path: basePath + 'sendmail',
        config: MainController.mail.sendMail()
      },
      {
        method: 'POST',
        path: basePath + 'welcomemail',
        config: MainController.mail.welcomeMail()
      },
      {
        method: 'POST',
        path: basePath + 'verificationmail',
        config: MainController.mail.verificationMail()
      },

    ]);
  },
  pkg: require('../../../package.json'),
  name: 'resource_routes_v1'
};