'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

/**
 * Location Schema
 */
var ProprietaireSchema = new Schema({
  fullname: { type: String },
  email: { type: String },
  phone: { type: String },
  agence: { type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },
}, {
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

Mongoose.model('Proprietaire', ProprietaireSchema);