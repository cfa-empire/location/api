//pictures: [{ type: String }],
// parrain: { type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },

'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

/**
 * Location Schema
 */
var LocationSchema = new Schema({
  address: {
    longitude: { type: Schema.Types.Mixed },
    latitude: { type: Schema.Types.Mixed },
  },
  pictures: [{ type: String }],
  zone: { type: String },
  price: { type: Number },
  message: { type: String },
  description: {
    nbreChambres: { type:  Number },
    nbreSalons: { type:  Number },
    nbreDouches: { type: Number },
    nbreCuisines: { type: Number },
    arriereCour: { type: Boolean },
    cuisineInterne: { type: Boolean },
    
    couloir: { type: Boolean },
    superficie: { type: Number },
    garage: { type: Boolean },

    sanitaire: { type: Boolean },
    nouvelleConstruction: { type: Boolean },
    genreLocation: {
        type: String,
        enum: ['Duplex','Entrée Couché','Triplex','Rez','R+1','R+2','R+3','R++'],
    },
    nbreMenages: { type: String },
    typeCompteur: {
      type: String,
      enum: ['Conventionnel', 'Carte']
    },
    eau: {
      type: String,
      enum: ['Forage et Soneb', 'Forage','Soneb']
    },
    aeration: {
      type: String,
      enum: ['Ventilateur', 'Climatiseur', 'Ventilateur et Climatiseur','Aucune']
    },
  },
  proprietaire:  { type: Mongoose.Schema.Types.ObjectId, ref: 'Proprietaire'},
  agence:        { type: Mongoose.Schema.Types.ObjectId, ref: 'Admin'},
}, {
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

Mongoose.model('Location', LocationSchema);