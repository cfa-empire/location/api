'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

/**
 * Historique Schema
 */
var HistoriqueSchema = new Schema({
  position: {
    longitude: { type: Schema.Types.Mixed },
    latitude: { type: Schema.Types.Mixed },
  },
  date: { type: Date },
  message: { type: String },

  admin: { type: String },
  location: { type: String },
  
}, {
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});


Mongoose.model('Historique', HistoriqueSchema);