'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const crypto = require('crypto');
var short = require('short-uuid');

/**
 * Admin Schema
 */
var AdminSchema = new Schema({
    //register client : fullname phone password address role
    //register admin : fullname phone password role
    //register agence : name phone password role

    //admin & client
    password: { type: String },
    fullname: { type: String, },
    phone:  { type: String , unique: true},
    //agence: { type: Mongoose.Schema.Types.ObjectId, ref: 'Agence' },
    
    //client
    address: {
        longitude: { type: Schema.Types.Mixed },
        latitude: { type: Schema.Types.Mixed },
    },
    
    //agence
    name: { type: String },
    ifu: { type: String },
    addressAgence: { type: String },
    active: { type: Boolean, default: false },
    rccm: { type: String },

    gerantFullname: { type: String },
    gerantPhone: { type: String },
    gerantEmail: { type: String },
    gerantPieceType: { type: String },
    gerantPieceNumber: { type: String },
    
    role: {
        type: String,
        enum: ['client', 'admin','agence'],
        default: 'admin'
    },

}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

/**
 * Hook a pre save method to hash the password
 */
AdminSchema.pre('save', function (next) {
    if (this.password && this.isModified('password')) {
        this.password = this.hashPassword(this.password);
    }
    next();
});

/**
 * Create instance method for hashing a password
 */
AdminSchema.methods.hashPassword = function (password) {
    var shaSum = crypto.createHash('sha256');
    shaSum.update(password);
    return shaSum.digest('hex');
};

/**
 * Create instance method for authenticating Admin
 */
AdminSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

// console.log( "Getting short : ",short.generate().slice(0, 8));
/*
AdminSchema.pre('save', function (next) {
    if (this._id == (null || undefined)) {
        next();
    } else {
        if (this.codeparrainage == (null || undefined)) {
            this.codeparrainage = short.generate().slice(0, 7);
            next();
        } else {
            next();
        }
    }
});
*/
Mongoose.model('Admin', AdminSchema);